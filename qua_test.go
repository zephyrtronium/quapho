package quapho_test

import (
	_ "embed"
	"encoding/csv"
	"strings"
	"testing"

	"gitlab.com/zephyrtronium/quapho"
)

//go:embed testdata/qualtrics-export.csv
var qualtricsExport string

func TestResponseIDs(t *testing.T) {
	r := csv.NewReader(strings.NewReader(qualtricsExport))
	rel, err := quapho.QualtricsIDs(r)
	if err != nil {
		t.Error(err)
	}
	if len(rel) != 1 {
		t.Errorf("wrong number of items in relation: want 1, got %d", len(rel))
	}
	if _, ok := rel["R_testid"]; !ok {
		t.Errorf("no entry for R_testid in relation")
	}
	if t.Failed() {
		t.Logf("relation contents:\n%q", rel)
	}
}
