package quapho

import (
	"encoding/csv"
	"fmt"
	"io"
	"strings"
)

// RelateSoundIDs relates Qualtrics response IDs to Phonic submission IDs.
// IDs not found in the Qualtrics relation are skipped.
func RelateSoundIDs(r *csv.Reader, qua map[string]QualtricsValues) (map[string][]string, error) {
	k, resps, err := phoCols(r)
	if err != nil {
		return nil, err
	}

	m := make(map[string][]string)
	for {
		row, err := r.Read()
		if err == io.EOF {
			return m, nil
		}
		if err != nil {
			return nil, fmt.Errorf("couldn't read Phonic data: %w", err)
		}
		id := row[k]
		if _, ok := qua[id]; !ok {
			// The ID is not in the relation.
			continue
		}
		for _, v := range resps {
			m[id] = append(m[id], row[v])
		}
	}
}

// phoCols gets the Qualtrics and Phonic response ID column indices.
func phoCols(r *csv.Reader) (int, []int, error) {
	hdr, err := r.Read()
	if err != nil {
		return -1, nil, fmt.Errorf("couldn't read Phonic header: %w", err)
	}
	k := -1
	var resps []int
	for i, v := range hdr {
		switch {
		case v == "Session ID", v == "responseId":
			k = i
		case strings.HasPrefix(v, "Q") && strings.HasSuffix(v, " Response ID"):
			resps = append(resps, i)
		}
	}
	return k, resps, nil
}
