module gitlab.com/zephyrtronium/quapho

go 1.18

require github.com/faiface/beep v1.1.0

require github.com/pkg/errors v0.9.1 // indirect
