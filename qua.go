package quapho

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
)

// QualtricsValues is a map of metadata and question IDs to their values.
type QualtricsValues map[string]string

// QualtricsIDs relates Qualtrics response IDs to responses. The returned
// map contains a map for each response ID, which in turn contains a map from
// each metadata item and question ID to its value.
func QualtricsIDs(r *csv.Reader) (map[string]QualtricsValues, error) {
	r.ReuseRecord = false
	hdr, err := r.Read()
	if err != nil {
		return nil, fmt.Errorf("couldn't read first row of Qualtrics data: %w", err)
	}
	k, err := quaCol(r)
	if err != nil {
		return nil, err
	}

	// The header row that we use to find the response ID should be the last
	// header row, so we can start grabbing data.
	rows, err := r.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("couldn't read Qualtrics data: %w", err)
	}
	m := make(map[string]QualtricsValues, len(rows))
	for _, row := range rows {
		v := make(QualtricsValues, len(row))
		for i := range row {
			v[hdr[i]] = row[i]
		}
		m[row[k]] = v
	}
	return m, nil
}

// quaCol finds the index of the column which gives the response ID in a
// Qualtrics CSV export.
func quaCol(r *csv.Reader) (int, error) {
	type data struct {
		ImportId string `json:"ImportId"`
	}
	for {
		row, err := r.Read()
		if err != nil {
			return -1, fmt.Errorf("couldn't read data finding Qualtrics header: %w", err)
		}
		// The first several rows of a Qualtrics CSV export is the field names
		// in various formats. We're looking for the one that looks like JSON.
		if !json.Valid([]byte(row[0])) {
			continue
		}
		var d data
		for k, v := range row {
			if err := json.Unmarshal([]byte(v), &d); err != nil {
				return -1, fmt.Errorf("couldn't unmarshal from %q in Qualtrics header: %w", v, err)
			}
			if d.ImportId == "_recordId" {
				return k, nil
			}
		}
		// We didn't find an entry for the response ID.
		return -1, fmt.Errorf("no response ID in Qualtrics header %s", row)
	}
}
