package quapho

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"time"

	"github.com/faiface/beep/wav"
)

// CopyAudio copies the WAV file corresponding to id in z, a Phonic media
// export, to w, and returns the duration of the audio file.
func CopyAudio(w io.Writer, z *zip.Reader, id string) (dur time.Duration, err error) {
	name := id + ".wav"
	f, err := z.Open(name)
	if err != nil {
		return dur, fmt.Errorf("couldn't open %s: %w", name, err)
	}
	b, err := io.ReadAll(f)
	if err != nil {
		return dur, fmt.Errorf("couldn't read data from %s: %w", name, err)
	}
	if _, err := w.Write(b); err != nil {
		return dur, fmt.Errorf("couldn't write data from %s: %w", name, err)
	}
	s, format, err := wav.Decode(bytes.NewReader(b))
	if err != nil {
		return dur, fmt.Errorf("couldn't read audio data from %s: %w", name, err)
	}
	defer s.Close()
	dur = format.SampleRate.D(s.Len())
	return dur, nil
}
