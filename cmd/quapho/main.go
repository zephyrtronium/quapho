package main

import (
	"archive/zip"
	"encoding/csv"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/zephyrtronium/quapho"
)

func main() {
	var qua, pho, media, out string
	flag.StringVar(&qua, "qua", "", "Qualtrics CSV export `path`")
	flag.StringVar(&pho, "pho", "", "Phonic.ai CSV export `path`")
	flag.StringVar(&media, "media", "", "Phonic.ai media zip `path`")
	flag.StringVar(&out, "out", "", "output `directory` root")
	flag.Parse()
	log.SetFlags(0)
	if qua == "" || pho == "" || media == "" || out == "" {
		log.Fatal("need all arguments for now")
	}

	qf, err := os.Open(qua)
	if err != nil {
		log.Fatal(err)
	}
	defer qf.Close()
	qr := csv.NewReader(qf)

	pf, err := os.Open(pho)
	if err != nil {
		log.Fatal(err)
	}
	defer pf.Close()
	pr := csv.NewReader(pf)

	mz, err := zip.OpenReader(media)
	if err != nil {
		log.Fatal(err)
	}
	defer mz.Close()

	if err := os.MkdirAll(out, 0777); err != nil {
		log.Fatal(err)
	}

	q, err := quapho.QualtricsIDs(qr)
	if err != nil {
		log.Fatal(err)
	}
	p, err := quapho.RelateSoundIDs(pr, q)
	if err != nil {
		log.Fatal(err)
	}

	for id, vals := range q {
		err := output(out, id, vals, &mz.Reader, p[id])
		if err != nil {
			log.Println(id, err)
		}
	}
}

func output(out, id string, vals quapho.QualtricsValues, z *zip.Reader, audio []string) error {
	outdir := filepath.Join(out, id)
	err := os.Mkdir(outdir, 0777)
	if err != nil && !errors.Is(err, fs.ErrExist) {
		return fmt.Errorf("couldn't prepare output at %s: %w", outdir, err)
	}
	for i, v := range audio {
		i++ // count output from 1
		if v == "" {
			log.Printf("no audio for %s Q%d", id, i)
			continue
		}
		name := filepath.Join(outdir, fmt.Sprintf("Q%d.wav", i))
		dur, err := writeaudio(z, name, v)
		if err != nil {
			log.Print(err)
			continue
		}
		vals[fmt.Sprintf("Phonic Q%d Duration (ms)", i)] = fmt.Sprint(dur.Milliseconds())
	}
	f, err := os.Create(filepath.Join(out, id, "values.json"))
	if err != nil {
		return fmt.Errorf("couldn't create Qualtrics data output: %w", err)
	}
	if err := json.NewEncoder(f).Encode(vals); err != nil {
		return fmt.Errorf("couldn't dump Qualtrics data: %w", err)
	}
	return nil
}

func writeaudio(z *zip.Reader, name, id string) (dur time.Duration, reterr error) {
	f, err := os.Create(name)
	if err != nil {
		return 0, err
	}
	defer func() {
		if err := f.Close(); err != nil {
			if reterr == nil {
				reterr = err
			} else {
				reterr = fmt.Errorf("%w (additional error closing output: %v)", reterr, err)
			}
		}
	}()
	return quapho.CopyAudio(f, z, id)
}
